<?php

namespace Greetik\YtvideoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Ytvideo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Greetik\YtvideoBundle\Entity\YtvideoRepository")
 * @ORM\Table(name="ytvideo")
 */
class Ytvideo {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\Length(max=255)
     * @var string
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="text", nullable=true)
     */
    private $comments;

    /**
     * @var integer
     *
     * @ORM\Column(name="numorder", type="integer", nullable=true)
     */
    private $numorder;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     * @Assert\Url( message = "{{ value }} no es una url válida")
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="idyt", type="string", length=255)
     */
    private $idyt;

    /**
     * @var integer
     *
     * @ORM\Column(name="item_id", type="integer", nullable=true)
     */
    private $itemid;

    /**
     * @Assert\Length(max=255)
     * @var string
     *
     * @ORM\Column(name="item_type", type="string", length=255)
     */
    private $itemtype;

    /**
     * @Assert\Length(max=255)
     * @var string
     *
     * @ORM\Column(name="platform", type="string", length=255, nullable=true)
     */
    private $platform;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Ytvideo
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return Ytvideo
     */
    public function setComments($comments) {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string 
     */
    public function getComments() {
        return $this->comments;
    }

    /**
     * Set numorder
     *
     * @param integer $numorder
     * @return Ytvideo
     */
    public function setNumorder($numorder) {
        $this->numorder = $numorder;

        return $this;
    }

    /**
     * Get numorder
     *
     * @return integer 
     */
    public function getNumorder() {
        return $this->numorder;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Ytvideo
     */
    public function setPath($path) {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath() {
        return $this->path;
    }

    /**
     * Set idyt
     *
     * @param string $idyt
     * @return Ytvideo
     */
    public function setIdyt($idyt) {
        $this->idyt = $idyt;

        return $this;
    }

    /**
     * Get idyt
     *
     * @return string 
     */
    public function getIdyt() {
        return $this->idyt;
    }

    /**
     * Set itemid
     *
     * @param integer $itemid
     *
     * @return Ytvideo
     */
    public function setItemid($itemid) {
        $this->itemid = $itemid;

        return $this;
    }

    /**
     * Get itemid
     *
     * @return integer
     */
    public function getItemid() {
        return $this->itemid;
    }

    /**
     * Set itemtype
     *
     * @param string $itemtype
     *
     * @return Ytvideo
     */
    public function setItemtype($itemtype) {
        $this->itemtype = $itemtype;

        return $this;
    }

    /**
     * Get itemtype
     *
     * @return string
     */
    public function getItemtype() {
        return $this->itemtype;
    }

    /**
     * Set platform
     *
     * @param string $platform
     *
     * @return Ytvideo
     */
    public function setPlatform($platform) {
        $this->platform = $platform;

        return $this;
    }

    /**
     * Get platform
     *
     * @return string
     */
    public function getPlatform() {
        return $this->platform;
    }

    public function getThumbnail() {
        switch ($this->platform) {
            case 'vimeo':
                $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$this->idyt.php"));
                return $hash[0]['thumbnail_large'];
        }

        return 'http://img.youtube.com/vi/' . $this->idyt . '/0.jpg';
    }
    
    public function getEmbedurl(){
        switch ($this->platform) {
            case 'vimeo':
                return 'https://player.vimeo.com/video/'. $this->idyt . '?autoplay=1';
        }

        return 'https://www.youtube.com/embed/' . $this->idyt . '?autoplay=1';
    }

}
