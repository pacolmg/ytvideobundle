<?php

namespace Greetik\YtvideoBundle\Service;

use Greetik\YtvideoBundle\Entity\Ytvideo;
use Greetik\YtvideoBundle\Form\Type\YtvideoType;
use Symfony\Component\Config\Definition\Exception\Exception;
use UploadHandler;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Toolsitems
 *
 * @author Paco
 */
class Tools {

    private $em;
    private $interfacetools;

    public function __construct($_entityManager, $_interfacetools) {
        $this->em = $_entityManager;
        $this->interfacetools = $_interfacetools;
    }

    /**
     * Get the videos
     * 
     * @param int $item_id The videos are associated to an item with this id
     * @param string $item_type It's the type of the item that the videos are associated to
     * @return an array with the videos
     * @author Pacolmg
     */
    public function getVideos($item_id, $item_type) {
        return $this->em->getRepository('YtvideoBundle:Ytvideo')->findByItem($item_id, $item_type);
    }

    /**
     * Get a video
     * 
     * @param int $id The id of the video
     * @return Ytvideo
     * @author Pacolmg
     */
    public function getVideo($id) {
        return $this->em->getRepository('YtvideoBundle:Ytvideo')->findOneById($id);
    }

    /**
     * Get the form to insert/edit a video
     * 
     * @return YtvideoType
     * @author Pacolmg
     *
      public function getForm($id){
      return new
      }

      /**
     * Get a video and the form to edit it
     * 
     * @param int $id The id of the video
     * @return Ytvideo
     * @author Pacolmg
     *    
      public function getVideoAndForm($id){
      return array($this->getVideo($id));
      }
     */

    /**
     * Persist the new video in the bd and set its order
     * 
     * @param int $url the url of the video
     * @param Ytvideo $ytvideo the video
     * @return string the id of the youtube video
     * @author Pacolmg
     */
    public function insertVideo($ytvideo, $url, $id, $type) {
        if (!$id)
            throw new Exception("No se pudo obtener el Identificador del elemento");
        if (!$type)
            throw new Exception("Tipo de Elemento desconocido");

        $id_yt = $this->getYtvideoIdFromUrl($url);
        if (!$id_yt)
            throw new Exception("No se pudo obtener el Identificador del vídeo");

        $ytvideo->setIdyt($id_yt);
        $ytvideo->setPlatform($this->getplatform($url));
        $ytvideo->setItemtype($type);
        $ytvideo->setItemid($id);
        $ytvideo->setNumorder($this->em->getRepository('YtvideoBundle:Ytvideo')->findMaxNumOrderByElem($type, $id) + 1);

        $this->em->persist($ytvideo);
        $this->em->flush();
    }

    /**
     * Persist the new/modified video in the bd and set its order
     * 
     * @param int $url the url of the video
     * @param Ytvideo $ytvideo the video
     * @return string the id of the youtube video
     * @author Pacolmg
     */
    public function modifyVideo($ytvideo, $url) {
        $id_yt = $this->getYtvideoIdFromUrl($url);
        if (!$id_yt)
            throw new Exception('No se pudo obtener el Identificador del vídeo');

        $ytvideo->setIdyt($id_yt);
        $ytvideo->setPlatform($this->getplatform($url));

        $this->em->persist($ytvideo);
        $this->em->flush();
    }

    /**
     * Remove the video
     * 
     * @param int id the Id of the Ytvideo
     * @author Pacolmg
     */
    public function dropVideo($id) {
        $video = $this->em->getRepository('YtvideoBundle:Ytvideo')->findOneById($id);
        if (!$video)
            throw new Exception("No se encontró el vídeo.");

        $this->em->remove($video);
        $this->em->flush();
        return $this->reorderVideos($video->getItemid(), $video->getItemtype());
    }

    protected function getplatform($url) {
        $pos = strpos($url, 'vimeo.com/');
        if ($pos !== false)
            return 'vimeo';

        return '';
    }

    /**
     * Get the id of a youtube video from a url
     * 
     * @param int $url the url of the video
     * @return string the id of the youtube video
     * @author Pacolmg
     */
    protected function getYtvideoIdFromUrl($url) {
        $pos = strpos($url, 'vimeo.com/');
        if ($pos !== false) {
            return substr($url, $pos + 10);
        }

        $pos = strpos($url, 'v=');
        if ($pos === false)
            return 0;

        return substr($url, $pos + 2, 11);
    }

    /**
     * Move an video from its old position to a new position
     * 
     * @param int $id The video
     * @param int $newposition the new position of the video
     * @author Pacolmg
     */
    public function moveVideo($id, $newposition) {
        return $this->interfacetools->moveItemElem('YtvideoBundle:Ytvideo', $id, $newposition);
    }

    /**
     * Order of the videos of an item, from 1 to n
     * 
     * @param int $item_id The images are associated to an item with this id
     * @param string $item_type It's the type of the item that the images are associated to
     * @return true/false
     * @author Pacolmg
     */
    public function reorderVideos($item_id, $item_type) {
        return $this->interfacetools->reorderItemElems($this->getVideos($item_id, $item_type));
    }

}
