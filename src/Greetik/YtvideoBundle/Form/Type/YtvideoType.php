<?php
    namespace Greetik\YtvideoBundle\Form\Type;
    
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\Form\Extension\Core\Type\UrlType;
    use Symfony\Component\Form\Extension\Core\Type\HiddenType;
    use Symfony\Component\Form\Extension\Core\Type\TextareaType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of YtvideoType
 *
 * @author Paco
 */
class YtvideoType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder->add('title')
                    ->add('comments', TextareaType::class, array('required'=>false))
                    ->add('path', UrlType::class)
                    ->add('id_item', HiddenType::class, array("mapped" => false))
                    ->add('type', HiddenType::class, array("mapped" => false))
                    ->add('id', HiddenType::class, array("mapped" => false));
    }
    
    public function getName(){
        return 'Ytvideo';
    }
    
    public function getDefaultOptions(array $options){
        return array( 'data_class' => 'Greetik\YtvideoBundle\Entity\Ytvideo');
    }
}

